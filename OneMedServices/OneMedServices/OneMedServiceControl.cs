﻿using System;
using System.Configuration;
using System.Threading;
using ProfileServices;
using Topshelf;
using Topshelf.Logging;
using log4net;

namespace OneMedServices
{
   public class OneMedServiceControl: ServiceControl
    {
       //private static readonly LogWriter _log = HostLogger.Get<OneMedServiceControl>();
       readonly ILog _log = LogManager.GetLogger(
                                     typeof(OneMedServiceControl));

        public OneMedServiceControl()
        {
            _log.Info("OneMedServiceControl initialised...");
        }

        public bool Start(HostControl hostControl)
        {
            var ShouldRunTestService = false;
            var ShouldRunStaffService = false;
            var ShouldRunStudentService = false;

            // check the app.config to see what services should be run
            if (ConfigurationManager.AppSettings["RunTestService"] != null)
            {
                bool.TryParse(ConfigurationManager.AppSettings["RunTestService"].ToString(), out ShouldRunTestService);
            }

            if (ConfigurationManager.AppSettings["RunStaffService"] != null)
            {
                bool.TryParse(ConfigurationManager.AppSettings["RunStaffService"], out ShouldRunStaffService);
            }

            if (ConfigurationManager.AppSettings["RunStudentService"] != null)
            {
                bool.TryParse(ConfigurationManager.AppSettings["RunStudentService"], out ShouldRunStudentService);
            }

            if (ShouldRunStaffService || ShouldRunStudentService || ShouldRunTestService)
            {
                _log.Info("Service Starting...");

                hostControl.RequestAdditionalTime(TimeSpan.FromSeconds(10));

                Thread.Sleep(1000);

                ThreadPool.QueueUserWorkItem(x =>
                {
                    var connectionString = Convert.ToString(ConfigurationManager.ConnectionStrings["DefaultConnection"]);
                    _log.InfoFormat(string.Format("The service is connecting to  {0}", connectionString));
                    _log.InfoFormat(string.Format("Process calling sync {0}", DateTime.Now));

                    if (ShouldRunStudentService)
                    {
                        _log.Info("Running student service");
                        var studentService = new StudentService();
                        studentService.DoSync(connectionString);
                    }

                    if (ShouldRunStaffService)
                    {
                        _log.Info("Running staff service");
                        var staffService = new StaffService();
                        staffService.DoSync(connectionString);
                    }

                    if (ShouldRunTestService)
                    {
                        _log.Info("Running test service");
                        var testService = new TestService();
                        testService.ReadStudentData(connectionString);
                    }

                    hostControl.Stop();
                });
            }
            else
            {
                _log.Info("No services are marked to run in app.config");
            }

            return true;
        }

        public bool Stop(HostControl hostControl)
        {
            _log.Info("OneMedService Stopped");

            return true;
        }

        public bool Pause(HostControl hostControl)
        {
            _log.Info("OneMedService Paused");

            return true;
        }

        public bool Continue(HostControl hostControl)
        {
            _log.Info("OneMedService Continued");

            return true;
        }
    }
}
