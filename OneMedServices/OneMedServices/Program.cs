﻿using System.IO;
using log4net.Config;
using Topshelf;
using Topshelf.Logging;

namespace OneMedServices
{
    class Program
    {
        private static void Main()
        {
            // logger initialization
            XmlConfigurator.Configure();
 
            var host = HostFactory.New(x =>
            {
                x.UseLog4Net();
                x.Service<OneMedServiceControl>();

                x.RunAsLocalSystem();
                x.SetDescription("OneMedServiceControl Description");
                x.SetDisplayName("OneMedService");
                x.SetServiceName("OneMedService");
            });

            host.Run();
        }
    }
}
