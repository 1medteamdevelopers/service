﻿using System;
using System.Data;
using System.Data.SqlClient;
using Topshelf.Logging;
namespace ProfileServices
{
    public class StudentService
    {
        static readonly LogWriter _log = HostLogger.Get<StudentService>();

        public StudentService()
        {
            _log.DebugFormat("StudentService initiated");
        }

        /// <summary>
        /// Updates the students table from Campus solutions
        /// </summary>
        public void DoSync(string connectionString)
        {
            _log.InfoFormat(string.Format("StudentService DoSync {0}", DateTime.Now));
            try
            {
                const string spStudentSync = "CS_Staging_DoSync";
               

                RunCommandAsynchronously(spStudentSync, connectionString);

                
            }
            catch (Exception ex)
            {
                _log.ErrorFormat(String.Format("Student sync error {0}", ex.Message));
            }

            _log.InfoFormat(String.Format("Student sync completed {0}", DateTime.Now));
        }


        private static void RunCommandAsynchronously(string commandText, string connectionString)
        {
            // Given command text and connection string, asynchronously execute 
            // the specified command against the connection. For this example, 
            // the code displays an indicator as it is working, verifying the  
            // asynchronous behavior.  
            using (var connection = new SqlConnection(connectionString))
            {
                try
                {
                    var totalRecords = new SqlParameter("@CS_TotalRecords", SqlDbType.Int)
                    {
                        Direction = ParameterDirection.Output
                    };

                    var totalNewRecords = new SqlParameter("@CS_TotalNewRecords", SqlDbType.Int)
                    {
                        Direction = ParameterDirection.Output
                    };

                    var totalErrorRecords = new SqlParameter("@CS_TotalErrorRecords", SqlDbType.Int)
                    {
                        Direction = ParameterDirection.Output
                    };

                    var totalFieldsToUpdated = new SqlParameter("@CS_TotalFieldsToUpdated", SqlDbType.Int)
                    {
                        Direction = ParameterDirection.Output
                    };

                    var totalStudentTableFieldUpdated = new SqlParameter("@S_TotalStudentTableFieldUpdated", SqlDbType.Int)
                    {
                        Direction = ParameterDirection.Output
                    };

                    var command = new SqlCommand(commandText, connection) {CommandType = CommandType.StoredProcedure, CommandTimeout = 3000};
                    command.Parameters.Add(totalRecords);
                    command.Parameters.Add(totalNewRecords);
                    command.Parameters.Add(totalErrorRecords);
                    command.Parameters.Add(totalFieldsToUpdated);
                    command.Parameters.Add(totalStudentTableFieldUpdated);

                    connection.Open();
                    IAsyncResult result = command.ExecuteNonQueryAsync();

                    // Although it is not necessary, the following code 
                    // displays a counter in the console window, indicating that  
                    // the main thread is not blocked while awaiting the command  
                    // results. 
                    int count = 0;
                    while (!result.IsCompleted)
                    {
                        count += 1;
                        _log.InfoFormat("Waiting ({0})", count);
                        // Wait for result to finish
                        System.Threading.Thread.Sleep(5000);
                    }
                    int totalRecordsValue = int.Parse(totalRecords.Value.ToString());
                    int totalNewRecordsValue = int.Parse(totalNewRecords.Value.ToString());
                    int totalErrorRecordsValue = int.Parse(totalErrorRecords.Value.ToString());

                    int totalFieldsToUpdatedValue = int.Parse(totalFieldsToUpdated.Value.ToString());

                    int totalStudentTableFieldUpdatedValue = int.Parse(totalStudentTableFieldUpdated.Value.ToString());

                    _log.InfoFormat("Student service complete - Total Student Records : {0}, Total New Student Records : {1}, Total Error Records adding new Records {2}, " +
                                    "Total student records to be updated : {3}, Total student fields to be updated {4}",
                                    totalRecordsValue, totalNewRecordsValue, totalErrorRecordsValue, totalFieldsToUpdatedValue, totalStudentTableFieldUpdatedValue);
                }
                catch (SqlException ex)
                {
                    _log.InfoFormat("Error ({0}): {1}", ex.Number, ex.Message);
                }
                catch (InvalidOperationException ex)
                {
                    _log.InfoFormat("Error: {0}", ex.Message);
                }
                catch (Exception ex)
                {
                    // You might want to pass these errors 
                    // back out to the caller.
                    _log.InfoFormat("Error: {0}", ex.Message);
                }
            }
        }

    }

}
