﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Topshelf.Logging;

namespace ProfileServices
{
    public class TestService
    {
        //private static Logger _log = new Logger();
        static readonly LogWriter _log = HostLogger.Get<StudentService>();

        public TestService()
        {
            //_log.DebugFormat("TestService initiated");
        }
        public void ReadStudentData(string connectionString)
        {
            const string queryString = "SELECT TOP 10 FirstName, Surname FROM Student;";
            //string connectionString = Convert.ToString(ConfigurationManager.ConnectionStrings["DefaultConnection"]);
            using (var connection = new SqlConnection(connectionString))
            {
                var command = new SqlCommand(queryString, connection);
                connection.Open();

                var reader = command.ExecuteReader();

                // Call Read before accessing data. 
                while (reader.Read())
                {
                    ReadSingleRow(reader);
                }

                // Call Close when done reading.
                reader.Close();
            }
        }

        private void ReadSingleRow(IDataRecord record)
        {
            //Console.WriteLine(String.Format("{0}, {1}", record[0], record[1]));
            _log.DebugFormat(String.Format("{0}, {1}", record[0], record[1]));
        }
    }
}
